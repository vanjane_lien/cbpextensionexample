//
//  TodayViewController.m
//  CBPTodayExtensionExample
//
//  Created by Karl Monaghan on 17/06/2014.
//  Copyright (c) 2014 Crayons and Brown Paper. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import <SpriteKit/SpriteKit.h>
#import <CoreLocation/CoreLocation.h>

@import CBPKit;

@interface TodayViewController () <NCWidgetProviding, UITableViewDelegate, CLLocationManagerDelegate> {
    SKTextureAtlas *atlas;
    SKSpriteNode *character;
    SKAction *characterWalk;
    SKScene *animScene;
    SKView *animView;
    
}
@property (nonatomic) CBPExtensionExampleDataSource *dataSource;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) UILabel *locationLabel;
//@property (nonatomic) UITableView *tableView;
@end

@implementation TodayViewController

- (void)loadView
{
    [super loadView];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    //[self.view addSubview:self.tableView];
    [self initAtlasAnimation];
}
#pragma mark - test
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    NSLog(@"WIDGET viewDidDisapper");
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:YES];
    NSLog(@"WIDGET viewWillDisapper");
    
}




- (void)viewDidLoad {
    [super viewDidLoad];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    // Do any additional setup after loading the view from its nib.
    
    /*[self.dataSource loadPosts:2 completion:^(NSError *error) {
        if (!error) {
            [self updateTableView];
        } else {
            NSLog(@"Download error: %@", error);
        }
    }];*/
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    [self.dataSource loadPosts:2 completion:^(NSError *error) {
        NCUpdateResult result = NCUpdateResultNoData;
        if (!error) {
            result = NCUpdateResultNewData;
            
            //[self updateTableView];
        } else {
            NSLog(@"Download error: %@", error);
            
            result = NCUpdateResultFailed;
        }
        
        completionHandler(result);
    }];
}


#pragma mark - Location delegate

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.locationLabel.text = [NSString stringWithFormat:@"%@", locations];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    self.locationLabel.text = @"Get Location Fail";
}
#pragma mark - SpriteKit
- (void) initAtlasAnimation {
    animView = [[SKView alloc] initWithFrame:CGRectMake(10.f, 0.f, 75.0, 130.0f)];
    [animView setBackgroundColor:[SKColor clearColor]];
    [self.view addSubview:animView];
    
    animScene = [[SKScene alloc] initWithSize:animView.bounds.size];
    [animScene setBackgroundColor:[SKColor clearColor]];
    
    character = [[SKSpriteNode alloc] initWithImageNamed:@"0001.png"];
    [character setSize:CGSizeMake(animScene.size.width, animScene.size.height)];
    [character setPosition:CGPointMake(animScene.size.width/2, animScene.size.height/2)];
    [animScene addChild:character];
    [animView presentScene:animScene];
    
    atlas = [SKTextureAtlas atlasNamed:@"character"];
    
    NSMutableArray *textureArr = [[NSMutableArray alloc]init];
    for (int i = 1; i<10; i++) {
        SKTexture *temp = [atlas textureNamed:[NSString stringWithFormat:@"000%d.png",i]];
        [textureArr addObject:temp];
    }
    
    characterWalk = [SKAction repeatActionForever:[SKAction animateWithTextures:textureArr timePerFrame:0.1f]];
    
    [character runAction:characterWalk];
    
    self.locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 130.f, 200.f, 30.f)];
    self.locationLabel.numberOfLines = 2;
    self.locationLabel.textAlignment = NSTextAlignmentCenter;
    self.locationLabel.textColor = [UIColor whiteColor];
    self.locationLabel.text = @"Here is the label";
    [self.view addSubview:self.locationLabel];
    [self setPreferredContentSize:CGSizeMake(320.0f, 150.f)];
}

#pragma mark -
/*- (void)updateTableView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView beginUpdates];
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
        
        [self.tableView endUpdates];
        
        CGFloat height = (self.tableView.contentSize.height) ? self.tableView.contentSize.height : [self.dataSource tableView:self.tableView numberOfRowsInSection:0] * CBPTodayTableViewCellHeight;
        
        [self setPreferredContentSize:CGSizeMake(self.tableView.contentSize.width, height)];
    });
    
    
}*/

#pragma mark - UITableViewDelegate
/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPExtensionExamplePost *post = [self.dataSource postAtIndex:indexPath.row];
    
    [self.extensionContext openURL:[NSURL URLWithString:[NSString stringWithFormat:@"cbpextensionexample://%@", @(post.postId)]]
                 completionHandler:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
}

#pragma mark -
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.frame];
        _tableView.delegate = self;
        _tableView.dataSource = self.dataSource;
        
        _tableView.rowHeight = CBPTodayTableViewCellHeight;
        _tableView.estimatedRowHeight = CBPTodayTableViewCellHeight;
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = [UIColor clearColor];
        
        [_tableView registerClass:[CBPExtensionExampleTableViewCell class] forCellReuseIdentifier:CBPExtensionExampleTableViewCellIdentifier];
    }
    
    return _tableView;
}*/

/*- (CBPExtensionExampleDataSource *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[CBPExtensionExampleDataSource alloc] initWithTodayCell];
    }
    
    return _dataSource;
}*/

@end
