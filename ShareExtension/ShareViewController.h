//
//  ShareViewController.h
//  ShareExtension
//
//  Created by Thuy Lien Nguyen on 9/29/14.
//  Copyright (c) 2014 Crayons and Brown Paper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : SLComposeServiceViewController

@end
